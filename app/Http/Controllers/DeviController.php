<?php

namespace App\Http\Controllers;

use App\Models\Devi;
use App\Http\Requests\StoreDeviRequest;
use App\Http\Requests\UpdateDeviRequest;

class DeviController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDeviRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDeviRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Devi  $devi
     * @return \Illuminate\Http\Response
     */
    public function show(Devi $devi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Devi  $devi
     * @return \Illuminate\Http\Response
     */
    public function edit(Devi $devi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDeviRequest  $request
     * @param  \App\Models\Devi  $devi
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDeviRequest $request, Devi $devi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Devi  $devi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Devi $devi)
    {
        //
    }
}
