<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devi extends Model
{
    use HasFactory;
    protected $table ="devi";
    protected $Fillable =[
        "id _client",
        "id_formation",
        "nombre_participant",
        "commentaire",
        "document_formation",
        
    ];

    
}
