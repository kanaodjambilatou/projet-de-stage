<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;
    protected $table ="formation";
    protected $Fillable =[
        "code",
        "intitulé",
        "description",
        "durée",
        "niveau",
        "type",
        "cible",
    ];

    public function client(){
        return $this ->belongsToMany (Client ::class);
    }
}
